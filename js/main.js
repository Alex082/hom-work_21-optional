//получаем кнопку по айди и скрываем после нажатия
const btn = document.getElementById("drawCircleBtn");

btn.addEventListener("click", function () {
  btn.style.display = "none";

  // добавляем инпут для ввода размера круга и удаляем все не числа
  const input = document.createElement("input");

  input.type = "text";
  input.placeholder = "Диаметр круга (например 1*1)";
  input.addEventListener("input", function () {
    input.value = input.value.replace(/[^0-9*]/g, "");
  });
  document.body.appendChild(input);

  // делаем команду нарисовать
  const drawBtn = document.createElement("button");

  drawBtn.textContent = "Намалювати";
  document.body.appendChild(drawBtn);

  //проверку на совпадения
  drawBtn.addEventListener("click", function () {
    const diameterStr = input.value;
    const match = diameterStr.match(/^(\d+)\*(\d+)$/);
    if (match) {
      const diameter = parseInt(match[1], 5) * parseInt(match[2], 5);
      if (!isNaN(diameter)) {
        const circleContainer = document.createElement("div");
        circleContainer.style.display = "flex";
        circleContainer.style.flexWrap = "wrap";
        document.body.appendChild(circleContainer);

        // указываем количество и вешаем стили
        const numCircles = 100;

        for (let i = 0; i < numCircles; i++) {
          const circle = document.createElement("div");
          circle.classList.add("circle");
          circle.style.width = `${diameter}px`;
          circle.style.height = `${diameter}px`;
          circle.style.borderRadius = "50%";
          circle.style.margin = "5px";
          circle.style.backgroundColor = getRandomColor();

          // говорим что если клик тогда этот круг или элемент удаляется с экрана
          circle.addEventListener("click", function () {
            circleContainer.removeChild(circle);

            //достаём все классы и идём циклем пока не достигнем длины и сравниваем
            const allCircles = document.querySelectorAll(".circle");
            const numCircles = allCircles.length;

            for (let i = 0; i < numCircles; i++) {
              if (allCircles !== circle) {
                allCircles[i].style.order = i + 1;
              }
            }
          });
          circleContainer.appendChild(circle);
        }
      }
    }
  });
});

// делаем функцыю рандомного выбора цвета
function getRandomColor() {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
